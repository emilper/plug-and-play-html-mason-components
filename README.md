# some plug-and-play HTML::Mason components

various HTML::Mason components which can be plugged in an application

##  Photo gallery

How to use it:

install <a href="https://metacpan.org/release/Imager">Imager</a>

put the pictures in jpeg format in the 'gallery_path' folder, and give permissions on that folder to the user of the web server;
when loaded first, the mason component will create the thumbs (width 100px) and the preview (width 400px) folders and create the thumbnails and the preview images.

How to call it in your page

 <&/lib/image_gallery.mas, 'gallery_path' => '/photos/city_colors', 'thumbs_list_width' => '3' &>
 
 
Credits: got ideas from <a href="http://www.perl.com/pub/2004/04/01/masongal.html">Photo Galleries with Mason and Imager</a> by Casey West

##  Current time

How to call it in your page

 <& lib/current_time.mas, 'locale' => 'Europe/Budapest', 'label' => 'Budapest' &>


## File last modified

How to use: put it in the autohandler and it will include the date when the component called by $m->call_next() was modified on disk. If no 'next_component_file_path' is passed, it will show the modification time for the 'file_last_modified.mas' component.

How to call it in your page

 <& lib/file_last_modified.mas, 'file' => $next_component_file_path, 'timezone' => 'Europe/Bucharest', 'timezone_label' => 'Bucharest'&>
